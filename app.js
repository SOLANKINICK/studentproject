const express = require('express')
const mysql = require('mysql2')
const bodyParser = require('body-parser')
const path = require('path')


const db = require('./config/database')

db.authenticate()
    .then(()=>console.log("Connection done"))
    .catch(err => console.log(err))

const app = express()
app.use(bodyParser.json())

app.get('/',(req,res)=>{
    res.send("Hi you are on student info page")
})

app.use('/api/students',require('./routes/userRoutes.js'))
app.use('/api/classes',require('./routes/classRoutes.js'))
app.use('/api/attendance',require('./routes/attendanceRoutes.js'))

const PORT = process.env.PORT || 4000

app.listen(PORT, console.log(`server started on port ${PORT}`))
