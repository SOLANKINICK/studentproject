const express = require('express')
const router = express.Router()
const db = require('../config/database.js')
const attData = require('../models/attandance.js')
const logger = require('../logger/loggerConfig.js')

router.get('/' , (req,res)=>{
    attData.findAll()
        .then((data)=>{
            res.status(200)
            res.send(data)
        })
        .catch((err)=>{
            logger.error(err)
            res.send(err)
        })
     }
)

router.get('/:Id',(req,res)=>{
    const ID = req.params['Id']
    attData.findByPk(ID)
        .then((info) =>{
            res.send(info)
        })
        .catch((err)=>{
            logger.error(err)
            res.send(err)
        })
    }
)

router.delete('/:Id',(req,res)=>{
    const ID = req.params['Id']
    attData.destroy({
        where : {
            userId :ID
        }
    })
    .then((result)=>{
        res.send("info has been deleted")
    })
    .catch((err)=>{
        logger.error(err)
        res.send(err)
    })
})

router.post('/',(req,res)=>{
    let {userId, classId,attendance } = req.body

    if(!userId){
        res.send("please put the userid and try again")
    }
    attData.create({
        userId,
        classId,
        attendance
    })
    .then((result)=>{
        res.send("information have been added")
    })
    .catch((err)=>{
        logger.error(err)
        res.send(err)
    })
})



module.exports = router
