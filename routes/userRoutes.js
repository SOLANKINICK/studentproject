const express = require('express')
const router = express.Router()
const db = require('../config/database.js')
const student = require('../models/user.js')
const logger = require('../logger/loggerConfig.js')

router.get('/' , (req,res)=>{
    student.findAll()
        .then((students)=>{
            res.status(200)
            res.send(students)
        })
        .catch((err)=>{
            logger.error(err)
            res.send(err)
        })
     }
)

router.get('/:Id',(req,res)=>{
    const ID = req.params['Id']
    student.findByPk(ID)
        .then((student) =>{
            res.send(student)
        })
        .catch((err)=>{
            logger.error(err)
            res.send(err)
        })
    }
)

router.delete('/:Id',(req,res)=>{
    const ID = req.params['Id']
    student.destroy({
        where : {
            userId :ID
        }
    })
    .then((result)=>{
        res.send("data has been deleted")
    })
    .catch((err)=>{
        logger.error(err)
        res.send(err)
    })
})

router.post('/',(req,res)=>{
    let { firstName,lastName,age,gender,motherName,fatherName,userId } = req.body

    if(!userId){
        res.send("please put the userid and try again")
    }
    student.create({
        firstName,
        lastName,
        age,
        gender,
        motherName,
        fatherName,
        userId
    })
    .then((result)=>{
        res.send("student information have been added")
    })
    .catch((err)=>{
        logger.error(err)
        res.send(err)
    })
})

router.patch('/:Id',(req,res)=>{
    const studentInfo = req.body
    //console.log(studentInfo.firstName)
    let studentObj = {}
    const id = req.params['Id']
        if(!studentInfo.firstName){
        studentObj.firstName = studentInfo.firstName
        }
        if(!studentInfo.lastName){
            studentObj.lastName = studentInfo.laststName
        }
        if(!studentInfo.age){
            studentObj.age = studentInfo.age
        }
        if(!studentInfo.gender){
            studentObj.gender = studentInfo.gender
        }
        if(!studentInfo.motherName){
            studentObj.motherName = studentInfo.motherName
        }
        if(!studentInfo.fatherName){
            studentObj.fatherName = studentInfo.fatherName
        }
    student.update(
        {
            studentObj    
        },
        {
            where : {
                userId : id
            }
        }
    )
    .then((result)=>{
        res.send("data have been updated")
    }) 
    .catch((err)=>{
        logger.error(err)
        res.send(err)
    })
})

module.exports = router