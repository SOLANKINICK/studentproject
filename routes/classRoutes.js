const express = require('express')
const router = express.Router()
const db = require('../config/database.js')
const classData = require('../models/class.js')
const logger = require('../logger/loggerConfig.js')


router.get('/' , (req,res)=>{
    classData.findAll()
        .then((classes)=>{
            res.status(200)
            res.send(classes)
        })
        .catch((err)=>{
            logger.error(err)
            res.send(err)
        })
     }
)

router.get('/:Id',(req,res)=>{
    const ID = req.params['Id']
   classData.findByPk(ID)
        .then((classInfo) =>{
            res.send(classInfo)
        })
        .catch((err)=>{
            logger.error(err)
            res.send(err)
        })
    }
)

router.delete('/:Id',(req,res)=>{
    const ID = req.params['Id']
    classData.destroy({
        where : {
            classid :ID
        }
    })
    .then((result)=>{
        res.send("class has been deleted")
    })
    .catch((err)=>{
        logger.error(err)
        res.send(err)
    })
})

router.post('/',(req,res)=>{
    let {classid, className } = req.body

    if(!classid){
        res.send("please put the userid and try again")
    }
    classData.create({
        classid,
        className
    })
    .then((result)=>{
        res.send("class information have been added")
    })
    .catch((err)=>{
        logger.error(err)
        res.send(err)
    })
})



module.exports = router
