const winston = require('winston')

const logConfiguration = {
    level: 'error',
    defaultMeta: { service: 'user-service' },
    'transports': [
        new winston.transports.File({
            filename: 'error.log'
        })],
}

module.exports = winston.createLogger(logConfiguration)
