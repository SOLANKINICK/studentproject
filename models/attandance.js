const Sequelize = require('sequelize')
const db = require('../config/database')

const attendance = db.define('attendance',
    {
        userId : {
            type : Sequelize.INTEGER,
            primaryKey : true
        },
        classId : {
            type : Sequelize.INTEGER
        },
        attendance : {
            type : Sequelize.STRING
        }

    },
    {
        freezeTableName : true,
        timestamps :false
    }
)

module.exports = attendance