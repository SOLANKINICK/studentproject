const Sequelize = require('sequelize')
const db = require('../config/database')

const classes = db.define('classes',
    {
        classid : {
            type : Sequelize.INTEGER,
            primaryKey : true
        },

        className : {
            type : Sequelize.STRING
        }
    },
    
    {
        freezeTableName : true,
        timestamps :false
    }
)

module.exports = classes