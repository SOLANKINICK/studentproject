const Sequelize = require('sequelize')
const db = require('../config/database.js')


const user = db.define('user', 
    {

        firstName :{
            type :  Sequelize.STRING

        },
        lastName : {
            type : Sequelize.STRING
        },
        age : {
            type : Sequelize.INTEGER
        },
        gender : {
            type : Sequelize.STRING
        },
        motherName : {
            type : Sequelize.STRING
        },
        fatherName : {
            type : Sequelize.STRING
        },
        userId : {
            type : Sequelize.INTEGER,
            primaryKey : true
        }
    },
    {
        freezeTableName: true,
        timestamps: false,
    }
)

module.exports = user